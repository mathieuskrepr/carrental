<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Car::class);
    }

    /**
     * Get the top featured cars
     * @todo they're not actually featured yet
     *
     * @param int $limit
     * @return mixed
     */
    public function getFeaturedCars(int $limit = 4)
    {
        return $this->getQueryBuilder()
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * Create a QueryBuilder with aliases and joins preset
     *
     * @param null $indexBy
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder($indexBy = null): QueryBuilder
    {
        return $this->createQueryBuilder('c', $indexBy)
            ->innerJoin('c.brand', 'b')
            ->innerJoin('c.carPowertrain', 'p')
            ->innerJoin('c.carType', 't')
            ->addSelect('b')
            ->addSelect('p')
            ->addSelect('t');
    }

    /**
     * Get comments with filters
     *
     * @param bool $deleted
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getFilterQueryBuilder(?array $filter = [])
    {
        $qb = $this->getQueryBuilder();

        // Todo filter logics

        return $qb;
    }

    /**
     * Make a Criteria for Entities which have not (yet) been deleted
     *
     * @return Criteria
     */
    public static function createNonDeletedCriteria(): Criteria
    {
        // Deletions can be scheduled in the future
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq('deletedAt', null))
            ->orWhere(Criteria::expr()->gt('deletedAt', new \DateTime()));
    }

    // /**
    //  * @return Car[] Returns an array of Car objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Car
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
