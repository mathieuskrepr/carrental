<?php

namespace App\Repository;

use App\Entity\RentalAppointment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RentalAppointment|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentalAppointment|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentalAppointment[]    findAll()
 * @method RentalAppointment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentalAppointmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RentalAppointment::class);
    }

    // /**
    //  * @return RentalAppointment[] Returns an array of RentalAppointment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RentalAppointment
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
