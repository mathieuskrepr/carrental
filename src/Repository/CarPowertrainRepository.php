<?php

namespace App\Repository;

use App\Entity\CarPowertrain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CarPowertrain|null find($id, $lockMode = null, $lockVersion = null)
 * @method CarPowertrain|null findOneBy(array $criteria, array $orderBy = null)
 * @method CarPowertrain[]    findAll()
 * @method CarPowertrain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarPowertrainRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CarPowertrain::class);
    }

    // /**
    //  * @return CarPowertrain[] Returns an array of CarPowertrain objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CarPowertrain
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
