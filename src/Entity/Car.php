<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarRepository")
 */
class Car
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $model;

    /**
     * @ORM\Column(type="date")
     */
    private $modelYear;

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     */
    private $registrationPlate;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"model"})
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CarBrand", inversedBy="cars")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CarType", inversedBy="cars")
     * @ORM\JoinColumn(nullable=false)
     */
    private $carType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CarPowertrain", inversedBy="cars")
     * @ORM\JoinColumn(nullable=false)
     */
    private $carPowertrain;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RentalAppointment", mappedBy="car")
     */
    private $rentalAppointments;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->rentalAppointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getModelYear(): ?\DateTimeInterface
    {
        return $this->modelYear;
    }

    public function setModelYear(\DateTimeInterface $modelYear): self
    {
        $this->modelYear = $modelYear;

        return $this;
    }

    public function getRegistrationPlate(): ?string
    {
        return $this->registrationPlate;
    }

    public function setRegistrationPlate(string $registrationPlate): self
    {
        $this->registrationPlate = $registrationPlate;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getBrand(): ?CarBrand
    {
        return $this->brand;
    }

    public function setBrand(?CarBrand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getCarType(): ?CarType
    {
        return $this->carType;
    }

    public function setCarType(?CarType $carType): self
    {
        $this->carType = $carType;

        return $this;
    }

    public function getCarPowertrain(): ?CarPowertrain
    {
        return $this->carPowertrain;
    }

    public function setCarPowertrain(?CarPowertrain $carPowertrain): self
    {
        $this->carPowertrain = $carPowertrain;

        return $this;
    }

    /**
     * @return Collection|RentalAppointment[]
     */
    public function getRentalAppointments(): Collection
    {
        return $this->rentalAppointments;
    }

    public function addRentalAppointment(RentalAppointment $rentalAppointment): self
    {
        if (!$this->rentalAppointments->contains($rentalAppointment)) {
            $this->rentalAppointments[] = $rentalAppointment;
            $rentalAppointment->setCar($this);
        }

        return $this;
    }

    public function removeRentalAppointment(RentalAppointment $rentalAppointment): self
    {
        if ($this->rentalAppointments->contains($rentalAppointment)) {
            $this->rentalAppointments->removeElement($rentalAppointment);
            // set the owning side to null (unless already changed)
            if ($rentalAppointment->getCar() === $this) {
                $rentalAppointment->setCar(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
