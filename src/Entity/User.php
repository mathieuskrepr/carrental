<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserData", mappedBy="user", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    private $userData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RentalAppointment", mappedBy="user")
     */
    private $rentalAppointments;

    public function __construct()
    {
        $this->rentalAppointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserData(): ?UserData
    {
        return $this->userData;
    }

    public function setUserData(UserData $userData): self
    {
        $this->userData = $userData;

        // set the owning side of the relation if necessary
        if ($this !== $userData->getUser()) {
            $userData->setUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|RentalAppointment[]
     */
    public function getRentalAppointments(): Collection
    {
        return $this->rentalAppointments;
    }

    public function addRentalAppointment(RentalAppointment $rentalAppointment): self
    {
        if (!$this->rentalAppointments->contains($rentalAppointment)) {
            $this->rentalAppointments[] = $rentalAppointment;
            $rentalAppointment->setUser($this);
        }

        return $this;
    }

    public function removeRentalAppointment(RentalAppointment $rentalAppointment): self
    {
        if ($this->rentalAppointments->contains($rentalAppointment)) {
            $this->rentalAppointments->removeElement($rentalAppointment);
            // set the owning side to null (unless already changed)
            if ($rentalAppointment->getUser() === $this) {
                $rentalAppointment->setUser(null);
            }
        }

        return $this;
    }
}
