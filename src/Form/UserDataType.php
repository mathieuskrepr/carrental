<?php

namespace App\Form;

use App\Entity\UserData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add(
                'birthday',
                DateType::class,
                [
                    // renders it as a single text box
                    'widget' => 'single_text',
                ]
            )
            ->add('street')
            ->add('houseNumber')
            ->add('postalCode')
            ->add('city')
            ->add('phone')
            ->add('validLicense');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => UserData::class,
            ]
        );
    }
}
