<?php
/**
 * CarFormType.php.
 */

namespace App\Form;

use App\Entity\Car;
use App\Entity\CarBrand;
use App\Entity\CarPowertrain;
use App\Entity\CarType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Car|null $car */
        $car = $options['data'] ?? null;
        $isEdit = $car instanceof Car && $car->getId();

        $builder
            ->add(
                'registrationPlate',
                TextType::class,
                [
                    'attr' => [
                        'placeholder' => 'AB-00-CD',
                    ],
                ]
            )
            ->add('model')
            ->add('modelYear', DateType::class, [
                'widget' => 'text',
            ])
            ->add(
                'carType',
                EntityType::class,
                [
                    'class' => CarType::class,
                    'choice_label' => 'name',
                ]
            )
            ->add(
                'brand',
                EntityType::class,
                [
                    'class' => CarBrand::class,
                    'choice_label' => 'name',
                ]
            )
            ->add(
                'carPowertrain',
                EntityType::class,
                [
                    'class' => CarPowertrain::class,
                    'choice_label' => 'name',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
