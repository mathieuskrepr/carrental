<?php

namespace App\DataFixtures;

use App\Entity\Car;
use App\Entity\CarBrand;
use App\Entity\CarPowertrain;
use App\Entity\CarType;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CarFixture extends BaseFixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(
            10,
            'main_cars',
            function () {
                $car = new Car();

                /** @var CarBrand $brand */
                $brand = $this->getReference(
                    'main_brands_' . $this->faker->numberBetween(0, 9)
                );
                $car->setBrand($brand);
                $car->setModel($this->faker->vehicleModel($brand->getName()));

                /** @var CarPowertrain $powertrain */
                $powertrain = $this->getReference(
                    'main_powertrains_' . $this->faker->numberBetween(
                        0,
                        count(CarRelationsFixtures::$powerTrains) - 1
                    )
                );
                $car->setCarPowertrain($powertrain);

                /** @var CarType $type */
                $type = $this->getReference(
                    'main_types_' . $this->faker->numberBetween(0, 9)
                );
                $car->setCarType($type);

                $car->setModelYear($this->faker->dateTimeBetween('-10 years', '-1 year'));

                $car->setRegistrationPlate($this->faker->vehicleRegistration('[A-Z]{2}-[0-9]{2}-[A-Z]{2}'));

                return $car;
            }
        );

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [CarRelationsFixtures::class];
    }
}
