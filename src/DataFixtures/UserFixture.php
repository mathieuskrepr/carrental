<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends BaseFixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(
            10,
            'main_users',
            function ($i) {
                $user = new User();
                $user->setEmail(sprintf('user%d@carrental.com', $i));

                $user->setPassword(
                    $this->passwordEncoder->encodePassword(
                        $user,
                        'test'
                    )
                );

                return $user;
            }
        );

        // Make a admin user
        $user = new User();
        $user->setEmail('admin@carrental.com');
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'test'
            )
        );
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $manager->flush();
    }
}
