<?php

namespace App\DataFixtures;

use App\Entity\CarBrand;
use App\Entity\CarPowertrain;
use App\Entity\CarType;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * This will create multiple of the Car entity its relations
 * The Car entity requires this to run first
 *
 * Class CarRelationsFixtures
 * @package App\DataFixtures
 */
class CarRelationsFixtures extends BaseFixture
{
    public static $powerTrains = [
        'diesel',
        'benzine',
        'hybride',
        'LPG',
        'elektrisch',
    ];

    /**
     * @param ObjectManager $manager
     */
    public function loadData(ObjectManager $manager)
    {

        // Create car brands
        $this->createMany(
            10,
            'main_brands',
            function () {
                $brand = new CarBrand();
                $brand->setName($this->faker->vehicleBrand);
                return $brand;
            }
        );

        // Create car powertrains
        $this->createMany(
            count(self::$powerTrains),
            'main_powertrains',
            function ($count) {
                $powertrain = new CarPowertrain();
                $powertrain->setName(self::$powerTrains[$count]);
                return $powertrain;
            }
        );

        // Create car types
        $this->createMany(
            10,
            'main_types',
            function () {
                $type = new CarType();
                $type->setName($this->faker->vehicleType);
                return $type;
            }
        );

        $manager->flush();
    }
}
