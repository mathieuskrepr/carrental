<?php
/**
 * AbstractRdwServiceice.php.
 */

namespace App\RDW;


use DateTime;

class AbstractRdwService
{
    /**
     * Accepts a string and returns a date
     *
     * @param string $string in YYYYMMDD
     * @return bool|DateTime
     */
    public function stringToDate(string $string)
    {
        return DateTime::createFromFormat('Ymd', $string)->settime(0,0);
    }
}
