<?php
/**
 * ApkCheckerRdwService.php.
 */

namespace App\RDW;


use DateTime;
use Exception;
use Nettob\Component\Rdw\Model\Kenteken;
use Nettob\Component\Rdw\Repository\KentekenRepository;

class ApkCheckerRdwService extends AbstractRdwService
{

    /**
     * @var KentekenRepository
     */
    private $repository;

    /**
     * @var Kenteken
     */
    private $kenteken;

    /**
     * ApkCheckerRdwService constructor.
     * @param KentekenRepository $repository
     */
    public function __construct(KentekenRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Request info by registration plate
     *
     * @param string $registration
     */
    public function getByRegistrationPlate(string $registration)
    {
        // Strip of separators and transform to uppercase
        $registration = preg_replace('/[^A-Za-z0-9 ]/', '', $registration);
        $registration = strtoupper($registration);

        $query = ['kenteken' => $registration];

        $kenteken = $this->repository->find($query);

        $this->kenteken = $kenteken;
    }

    /**
     * Get the APK date, and parse into DateTime
     *
     */
    public function getApkExpireDate()
    {
        if (empty($this->kenteken)) {
            return null;
        }
        $rawDate = $this->kenteken->getVervaldatumApk();
        return $this->stringToDate($rawDate);
    }

    /**
     * Check if Apk is still valid today
     *
     */
    public function isApkValid()
    {
        if (empty($this->kenteken)) {
            return null;
        }
        $expDate = $this->getApkExpireDate();
        $today = new DateTime();
        $today->settime(0,0);

        return $today <= $expDate;
    }
}
