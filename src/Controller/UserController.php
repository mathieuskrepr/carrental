<?php
/** UserController - doet UserController dingen */
namespace App\Controller;

use App\Entity\User;
use App\Entity\UserData;
use App\Form\UserDataType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * The User's main page
     *
     * @Route("/profile", name="user_index")
     * @IsGranted("ROLE_USER")
     */
    public function index()
    {
        $user = $this->getUser();

        return $this->render(
            'user/index.html.twig',
            [
                'user' => $user,
            ]
        );
    }

    /**
     * @Route("/profile/edit", name="user_edit")
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, EntityManagerInterface $manager)
    {
        /** @var User $user */
        $user = $this->getUser();

        $userData = $user->getUserData() ?? new UserData();

        $userDataForm = $this->createForm(UserDataType::class, $userData);

        $userDataForm->handleRequest($request);
        if ($userDataForm->isSubmitted() && $userDataForm->isValid()) {
            /** @var UserData $userData */
            $userData = $userDataForm->getData();
            $userData->setUser($this->getUser());
            $manager->persist($userData);
            $manager->flush();

            $this->addFlash('success', 'Gegevens bijgewerkt');
        }

        return $this->render(
            'user/edit.html.twig',
            [
                'user' => $user,
                'userData' => $userData,
                'userDataForm' => $userDataForm->createView(),
            ]
        );
    }

    /**
     * List of users
     *
     * @Route("/profile/list", name="user_list")
     * @IsGranted("ROLE_ADMIN")
     */
    public function list(UserRepository $repository)
    {
        //todo admin only

        return $this->render(
            'user/list.html.twig',
            [
//            'user' => $user,
            ]
        );
    }
}
