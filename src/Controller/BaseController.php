<?php
/**
 * BaseController.php.
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends AbstractController
{

    /**
     * Serialise data, return as JSON with proper header
     *
     * @param $data
     * @param int $statusCode
     * @return JsonResponse
     */
    protected function createApiResponse($data, $statusCode = 200): JsonResponse
    {
        $json = $this->get('serializer')
                     ->serialize($data, 'json');

        return new JsonResponse($json, $statusCode, [
            'Content-Type' => 'application/json',
        ], true);
    }
}
