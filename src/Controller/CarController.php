<?php

namespace App\Controller;

use App\Entity\Car;
use App\Form\CarFormType;
use App\Repository\CarRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends AbstractController
{
    /**
     * Homepage
     *
     * @Route("/", name="car_index")
     */
    public function index(CarRepository $repository)
    {
        $cars = $repository->getFeaturedCars(4);

        //todo
        return $this->render(
            'car/index.html.twig',
            [
                'cars' => $cars,
            ]
        );
    }

    /**
     * Display a list of cars for public users
     *
     * @Route("/list", name="car_list")
     */
    public function list(CarRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        $criteria = $repository::createNonDeletedCriteria();

        $pagination = $paginator->paginate(
            $repository->getFilterQueryBuilder()->addCriteria($criteria),
            $request->query->getInt('page', 1),
            8
        );

        return $this->render(
            'car/list.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }

    /**
     * @Route("/car/{slug}", name="car_show")
     * @param Car $car
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Car $car)
    {
        return $this->render(
            'car/show.html.twig',
            [
                'car' => $car,
            ]
        );
    }
}
