<?php

namespace App\Controller;

use App\Entity\Car;
use App\Form\CarFormType;
use App\RDW\ApkCheckerRdwService;
use App\Repository\CarRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for all Car CRUDs
 *
 * @package App\Controller
 */
class AdminCarController extends BaseController
{
    /**
     * Show menu
     *
     * @Route("/admin/car", name="admin_car")
     */
    public function index(CarRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        $pagination = $paginator->paginate(
            $repository->getFilterQueryBuilder(),
            $request->query->getInt('page', 1),
            8
        );

        return $this->render('admin_car/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/admin/car/create", name="admin_car_create")
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(CarFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Car $car */
            $car = $form->getData();
            $em->persist($car);
            $em->flush();

            $this->addFlash('success', 'Car created!');

            return $this->redirectToRoute('admin_car_show', ['slug' => $car->getSlug()]);
        }

        return $this->render(
            'admin_car/car.create.html.twig',
            [
                'carForm' => $form->createView(),
            ]
        );
    }

    /**
     * Soft delete a car
     *
     * @Route("/admin/car/delete/{slug}", name="admin_car_delete")
     */
    public function delete(Car $car, EntityManagerInterface $manager)
    {
        // Set deletion date to NOW
        $car->setDeletedAt(new \DateTime());
        $manager->persist($car);
        $manager->flush();

        $this->addFlash('warning', 'Item deleted successfully');

        return $this->redirectToRoute('admin_car');
    }

    /**
     * Show car information in a form, so any data can be updated immediately
     *
     * @Route("/admin/car/show/{slug}", name="admin_car_show")
     */
    public function show(Car $car, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(CarFormType::class, $car);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Car $car */
            $car = $form->getData();
            $em->persist($car);
            $em->flush();

            $this->addFlash('success', 'Car updated!');

            // Redirect to self; this catches slug changes
            return $this->redirectToRoute('admin_car_show', ['slug' => $car->getSlug()]);
        }

        return $this->render('admin_car/car.show.html.twig', [
            'car' => $car,
            'carForm' => $form->createView(),
        ]);
    }

    /**
     *
     *
     * @Route("/admin/car/apk/{slug}", name="admin_car_apk")
     *
     * @param Car $car
     * @param ApkCheckerRdwService $apkChecker
     * @return Response
     */
    public function checkApk(Car $car, ApkCheckerRdwService $apkChecker, Request $request) {
        $apkChecker->getByRegistrationPlate($car->getRegistrationPlate());

        if ($request->getMethod() === 'POST') {
            $response = [
                'valid' => $apkChecker->isApkValid(),
                'date' => $apkChecker->getApkExpireDate()
            ];
            return $this->createApiResponse($response);
        }

        return $this->render('admin_car/car.apk.html.twig', [
            'car' => $car,
            'apkChecker' => $apkChecker
        ]);
    }
}
