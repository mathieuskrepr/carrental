<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\RentalAppointment;
use App\Entity\User;
use App\Entity\UserData;
use App\Form\UserDataType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RentalController extends AbstractController
{
    /**
     * Overview of the User's current Rentals
     *
     * @Route("/verhuur", name="rental_index")
     */
    public function index()
    {
        return $this->render(
            'rental/index.html.twig',
            [
                'controller_name' => 'RentalController',
            ]
        );
    }

    /**
     * Car rental step 1: Validate user data
     *
     * @Route("/verhuur/valideer/{slug}/", name="rental_validate")
     * @IsGranted("ROLE_USER")
     * @param Car $car
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function user(Car $car, Request $request, EntityManagerInterface $manager)
    {
        /** @var User $user */
        $user = $this->getUser();
        // If the user doesn't have existing UserData, create a new UserData
        $userData = $user->getUserData() ?? new UserData();
        $userDataForm = $this->createForm(UserDataType::class, $userData);

        // todo validate
        $userDataForm->handleRequest($request);
        if ($userDataForm->isSubmitted() && $userDataForm->isValid()) {
            // Success; save userData and continue
            /** @var UserData $userData */
            $userData = $userDataForm->getData();
            $userData->setUser($this->getUser());
            $manager->persist($userData);

            $rentalAppointment = new RentalAppointment();
            $rentalAppointment
                ->setUser($user)
                ->setCar($car)
                ->setConfirmed(false);
            $manager->persist($rentalAppointment);
            $manager->flush();

            // Flash success and redirect to next step
            $this->addFlash('success', 'Gegevens bijgewerkt');

            return $this->redirectToRoute('rental_period', [
                'slug' => $car->getSlug(),
                'appointment' => $rentalAppointment->getId(),
            ]);
        }

        return $this->render(
            'rental/user.html.twig',
            [
                'car' => $car,
                'userDataForm' => $userDataForm->createView(),
            ]
        );
    }

    /**
     * Rental step 2: rental period
     *
     * @Route("/verhuur/periode/{slug}/{appointment}", name="rental_period")
     * @IsGranted("ROLE_USER")
     * @TODO make voter to check if userData is valid
     * @param Car $car
     * @param RentalAppointment $appointment
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function period(Car $car, RentalAppointment $appointment)
    {
        //todo

        return $this->render(
            'rental/period.html.twig',
            [
                'car' => $car,
                'appointment' => $appointment,
            ]
        );
    }

    /**
     * Rental step 3: Confirmation
     *
     * @Route("/verhuur/confirm/{slug}/", name="rental_confirm")
     * @IsGranted("ROLE_USER")
     * @param Car $car
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function confirm(Car $car)
    {
        //todo

        return $this->render(
            'rental/confirm.html.twig',
            [
                'car' => $car,
            ]
        );
    }
}
