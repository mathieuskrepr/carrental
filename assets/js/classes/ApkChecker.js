'use strict';

const $ = require('jquery');
require('bootstrap');

const template = (data) => `
<div class="alert alert-${data.valid === null ? 'warning' : data.valid ? 'success' : 'danger'}" role="alert">
    <h4 class="alert-heading text-center">APK Status</h4>
    <div class="popover-body"></div>
</div>
`;

const contentTemplate = (data) => `
<hr>
<p class="mb-0">
    APK Verloopdatum: ${ (data.date === null ? 'Onbekend' : ApkChecker.stringDateConverter(data.date))}
</p>`;

class ApkChecker {
    constructor($wrapper) {
        this.$wrapper = $wrapper;

        this.$wrapper.find('.apk-checker')
            .on('click',
                this._openApkInfo.bind(this)
            );
    }

    _openApkInfo(event) {
        event.preventDefault();

        const $target = $(event.currentTarget);
        const url = $target.attr('href');

        $target.css('cursor', 'progress');

        this._fetchData(
            url
        ).then((data) => {
            this._openPopover($target, data);
            $target.css('cursor', '');
        }).catch((error) => {
            console.error(error);
        });
    }

    _fetchData(url) {
        return new Promise((resolve, reject) => {
            $.post({
                'url': url,
            }).then((data) => {
                resolve(data);
            }).catch((jqXHR) => {
                const errorData = jqXHR.responseText;

                reject(errorData);
            })
        })
    }

    _openPopover($target, data) {
        $($target)
            .popover({
                'trigger': 'focus',
                'html': true,
                'content': contentTemplate(data),
                'template': template(data)
            })
            .popover('toggle');
    }

    //todo use Moment.js
    static stringDateConverter(string) {
        const date = new Date(string);
        return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
    }
}

module.exports = ApkChecker;
