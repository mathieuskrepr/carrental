'use strict';

const ApkChecker = require('./classes/ApkChecker');
const $ = require('jquery');

$(document).ready(function () {
    const $wrapper = $('.car-info-table');
    window.checker = new ApkChecker($wrapper);
});
